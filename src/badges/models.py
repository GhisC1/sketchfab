from django.contrib.auth.models import User
from django.db import models


class Star(models.Model):
    """
    Requirement: one of your models as been viewed more than a 100 times
    """
    user = models.OneToOneField(User, primary_key=True)

    def __str__(self):
        return 'Star badge: one of your models as been viewed more than a 100 times'


class Collector(models.Model):
    """
    Requirement: you uploaded 5 models
    """
    user = models.OneToOneField(User, primary_key=True)

    def __str__(self):
        return 'Collector badge: you uploaded 5 models'


class Pioneer(models.Model):
    """
    Requirement: you have joined sketchfab more than a year ago
    """
    user = models.OneToOneField(User, primary_key=True)

    def __str__(self):
        return 'Pioneer badge: you have joined sketchfab more than a year ago'


class Heavy(models.Model):
    """
    Requirement:
    """
    user = models.OneToOneField(User, primary_key=True)

    def __str__(self):
        return 'Heavy badge: you uploaded an heavy object'
