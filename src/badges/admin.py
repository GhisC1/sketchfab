from django.contrib import admin

from .models import Star, Collector, Pioneer


admin.site.register(Star)
admin.site.register(Collector)
admin.site.register(Pioneer)
