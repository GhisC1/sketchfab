from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.models import User

from badges.models import Star, Collector, Pioneer


class UserListView(ListView):
    model = User
    template_name = 'users/user_list.html'


class UserDetailView(DetailView):
    model = User
    template_name = 'users/user_detail.html'
    slug_field = 'username'
    slug_url_kwarg = 'username'
