from datetime import timedelta

from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_in
from django.dispatch import receiver
from django.utils import timezone

from badges.models import Pioneer


def pioneer_badge(sender, **kwargs):
    """
    Signal to create a Pioneer badge when a user logged in and is a member since a year or more
    We could also create a daily task instead
    """
    user = kwargs['user']
    if timezone.now() - user.date_joined >= timedelta(days=365) \
            and not Pioneer.objects.filter(user=user).exists():
        Pioneer.objects.create(user=user)

user_logged_in.connect(pioneer_badge)
