from django.contrib.auth.models import User
from django.views.generic.base import View
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from .forms import ModelCreateForm
from .models import Model
from .processing import Processor
from badges.models import Star, Collector, Heavy


class ModelCreateView(CreateView):
    form_class = ModelCreateForm
    model = Model
    success_url = '/'

    def form_valid(self, form):
        model = form.save(commit=False)
        user = User.objects.get(pk=self.request.user.pk)
        model.user = user
        model.save()

        processor = Processor()
        processor.configure(model.file.path)
        weight = processor.weigh()

        # Heavy badge
        if weight >= 20 and not Heavy.objects.filter(user=user).exists():
            Heavy.objects.create(user=user)

        model.weight = weight
        model.vertice_count = processor.count_vertices()
        model.save()

        # Collector badge
        if user.models.count() >= 5 and not Collector.objects.filter(user=user).exists():
            Collector.objects.create(user=model.user)

        return super(ModelCreateView, self).form_valid(form)


class ModelListView(ListView):
    queryset = Model.objects.all().order_by('-creation_date')


class ModelsView(View):
    def post(self, request, *args, **kwargs):
        return ModelCreateView.as_view()(request)

    def get(self, request, *args, **kwargs):
        return ModelListView.as_view()(request)


class ModelDetailView(DetailView):
    model = Model
    slug_field = 'name'
    slug_url_kwarg = 'name'

    def get_object(self):
        """
        Overriding get_object() method to update the Model's views count
        """
        model = super(ModelDetailView, self).get_object()
        model.view_count += 1
        model.save()

        # Star badge
        if model.view_count >= 100 and not Star.objects.filter(user=model.user).exists():
            Star.objects.create(user=model.user)

        return model
